Journal Entries - Kapil Adhikari
Jul 28, 2023
Today, I worked on:
Finish all remaining MVP frontend needs
Do final “big” merge prior to testing / debugging our app
Jan 27, 2023
Today, I worked on:
Worked on the unit tests, navigation bar
Some access of sidebar
Jul 20, 2023
Today, I worked on:
Give access token for the navbar, Any user can't login without account, but now it need to login to homepage with username
Add user's name in the left top of side bar when they login
logout bottom will dispared before login
start on the unit test part
Jul 17, 2023
Today, I worked on:
debugging the update billings part
Jul 11, 2023
Today, I worked on:
Fix the bug of auth token in billings page
user's billings page working and it can return the token when they submit payment
Find the bug of pay rent, when user in login page, they couldn't use the navbar(css problem)
Jul 10, 2023
Today, I worked on:
billings page could return the Token when user submit payment to account. need some works to be done.
Try to add the pay rent bottom in the tenant page
learn how to add the pay rent  on the bottom& bottom of billings when user login
Jun 30, 2023
Today, I worked on:
Debuging the billings page, and test the frontend could return token when user login
User can get the token when they signup
connect the signup page in the sign in page
Jun 29, 2023
Today, I worked on:
Finished the Side Navbar design, and testing the connection with different icon
Review billings for adding the token inside Navbar

Jun 28, 2023
Today, I worked on:
Testing billings authentication backend - The backend is able to login /logout and sign up users
Start to working on the frontend design for billings
Start to build up the sidebar/ navbar design
Jun 28, 2023
Start to build up the authapi.py
Setup the token for different billings
Merging files in Gitlab
Keep working on the authapi
Jun 27, 2023
Today, I worked on:
Create POST endpoint for our billings service
Could see the user data from the pgAdmin
Working on the pool import statement
Start to discuss the frontend with team
Try to review the JWT-DOWN auth
Jun 26, 2023
Today, I worked on:
Create user billings endpoints
Created the first branch to and merged to main. some problem show up when the pipeline working
Creating our migrations files for users
Jun 23, 2023
Today, I worked on:
Createing our database and mapping table detail out in drawSQL
setting the Docker files, try to make the Docker running
Review and aligning on the lastest update wireframes